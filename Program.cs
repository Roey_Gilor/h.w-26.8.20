﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_26._8._20
{
    class Program
    {
        static void Main(string[] args)
        {
            MyQueue queue = new MyQueue();
            queue.Enqueue(new Customer(1, "ben", 2000, "hamelech", 20, 100));
            queue.Enqueue(new Customer(2, "dor", 1999, "harimon", 14, 30));
            queue.Enqueue(new Customer(3, "dana", 1980, "zamir", 5, 300));
            queue.Enqueue(new Customer(4, "danny", 1995, "hai", 12, 220));
            queue.Enqueue(new Customer(5, "adam", 2002, "hanasi", 20, 20));
            queue.Enqueue(new Customer(6, "dora", 1987, "gordon", 20, 620));
            queue.Dequeue();
            Customer customer = queue.WhoIsNext();
            List<Customer>cust= queue.DequeueCustomers(2);
            customer = queue.DequeueProtectzia();
            queue.SortByProtection();
            queue.SortByTotalPurchases();
            queue.SortByYearBirth();
            queue.AniRakSheela((new Customer(6, "edna", 2005, "hamelech", 18, 1000)));
            List<Customer> customers = new List<Customer>()
            {
                new Customer(1, "ran", 2001, "sharonim", 11, 900),
                new Customer(2, "shani", 2002, "hamelachim", 19, 800),
            };
            queue.Init(customers);
            queue.Clear();
        }
    }
}
