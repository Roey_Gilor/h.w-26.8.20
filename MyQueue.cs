﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_26._8._20
{
    class MyQueue
    {
        private List<Customer> customers;
        public MyQueue()
        {
            customers = new List<Customer>();
        }
        public void Enqueue(Customer customer)
        {
            customers.Add(customer);
        }
        public Customer Dequeue()
        {
            if (Count == 0)
                return null;
            Customer customer = customers[0];
            customers.RemoveAt(0);
            return customer;
        }
        public void Init(List<Customer> customers)
        {
            this.customers = customers;
        }
        public void Clear()
        {
            customers.Clear();
        }
        public Customer WhoIsNext()
        {
            return customers[0] == null ? null : customers[0];
        }
        public int Count
        {
            get
            {
                return customers.Count;
            }
        }
        public void SortByProtection()
        {
            customers.Sort(new ProtectionSort());
        }
        public void SortByTotalPurchases()
        {
            customers.Sort(new PurchasesSort());
        }
        public void SortByYearBirth()
        {
            customers.Sort(new YearBirthSort());
        }
        public List<Customer> DequeueCustomers (int num)
        {
            if (Count < num)
                num = num - (num - customers.Count);
            List<Customer> returnedCustomers = new List<Customer>();
            returnedCustomers = customers.GetRange(0, num);
            customers.RemoveRange(0, num);
            return returnedCustomers;
        }
        public void AniRakSheela(Customer customer)
        {
            customers.Insert(0, customer);
        }
        public Customer DequeueProtectzia()
        {
            int biggestProtection = -1;
            Customer customer = null;
            foreach (Customer item in customers)
            {
                if (item.Protection > biggestProtection)
                {
                    biggestProtection = item.Protection;
                    customer = item;
                }
            }
            return customer;
        }
    }

     class ProtectionSort : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.Protection.CompareTo(y.Protection);
        }
    }
    class PurchasesSort : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.TotalPurchases.CompareTo(y.TotalPurchases);
        }
    }
    class YearBirthSort : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.BirthYear.CompareTo(y.BirthYear);
        }
    }
}
